<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
</head>
<style>
body {
  margin:0px;
  padding:0px;

}
.overlay{
  padding:0px;
  margin:0px;
  height:100%;
  width:100%;
  overflow:none;
}

.wrapper{
  padding-top:100px;
}

.main-container{
  display: flex;
  justify-content: center;
  background-color: transparent;
  flex-wrap: wrap;
}

.main-container > div{
  background-color:rgba(255,255,255,0.6);
  width: 300px;
  margin: 7px;
  text-align: center;
  color:black;
  border-radius:10px;
}

.flex-container {
  display: flex;
  justify-content: center;
  background-color: transparent;
  flex-wrap: wrap;
}

.flex-container > div {
  background-color:rgba(255,255,255,0.6);
  width: 220px;
  margin: 7px;
  text-align: center;
  color:black;
  border-radius:10px;

}

h1{
  font-size:25px;
}

p{
  font-size:19px;
  line-height: 0px;
  padding:none;
}
</style>

<?php

$sixteenday="http://api.openweathermap.org/data/2.5/forecast?zip=97478,us&APPID=9f461f4c528a682ea86e31ebcfc7a39d";
$urlforcast="http://api.openweathermap.org/data/2.5/weather?zip=97478,us&APPID=9f461f4c528a682ea86e31ebcfc7a39d";
$json=file_get_contents($urlforcast);
$secondjson=file_get_contents($sixteenday);
$data=json_decode($json,true);
$seconddata=json_decode($secondjson,true);

//$w = $data['weather']['main'];

//$temp = $temp * 9/5 - 459.67;
//$temp = round($temp, 2);
$dates = array();

//var_dump($seconddata['list'][1]['main']['temp']);


foreach($seconddata['list'] as $d)
{
  $timestamp = $d['dt'];
  $date = date('F j, Y',$timestamp);
  array_push($dates, $date);
}

$new = array_unique($dates);
$final = array();
foreach($new as $n)
{
  array_push($final, $n);
}

function getHourlyData($val, $seconddata, $final)
{
  echo "<img style='height:100px; padding-top:10px;' src='images/".$seconddata['list'][$val]['weather'][0]['icon'].".png'>";
  echo "<h1>".$final[$val]."</h1>";
  echo "<h1>".$seconddata['list'][$val]['weather'][0]['description']."</h1>";
  foreach($seconddata['list'] as $d)
  {
    $t = $d['dt'];
    $time = date('g:i a', $t);
    $t = date('F j, Y',$t);
    $temp = $d['main']['temp'];
    $temp = $temp * 9/5 - 459.67;
    $temp = round($temp, 2);
    if($t == $final[$val]){

    echo "<p>".$time.": ".$temp."&#8457;</p>";
    }
  }
}

function displayDaysData()
{

}
$w = $data['weather'][0]['main'];
$weather = "".$w.".jpg";
$icon = $data['weather'][0]['icon'];
$temp = $data['main']['temp'];
$temp = $temp * 9/5 - 459.67;
$temp = round($temp, 2);
?>
<div style="height:100%; width:100%; background: url('images/<?php echo $weather ?>') no-repeat center center/cover;">
  <?php
echo "<div class='wrapper'>";
echo "<div class='main-container animated zoomIn'>";
echo "<div>";
echo "<h2>Todays Weather</h2>";
echo "<img style='height:150px;' src='images/".$icon.".png'>";
echo "<h3>".$data['weather'][0]['description']."</h3>";
echo "<h2>".$temp."&#8457;</h2>";
echo "</div>";
echo "</div>";

echo "<div class='flex-container animated zoomIn'>";

echo "<div>";
getHourlyData(0, $seconddata, $final);
echo "</div>";

echo "<div>";
getHourlyData(1, $seconddata, $final);
echo "</div>";

echo "<div>";
getHourlyData(2, $seconddata, $final);
echo "</div>";

echo "<div>";
getHourlyData(3, $seconddata, $final);
echo "</div>";

echo "<div>";
getHourlyData(4, $seconddata, $final);
echo "</div>";
echo "</div>";
?>

</div>

<?php




/*echo "<h1>Current Weather</h1>";
echo " ".$data['name']. "<br/>";
echo "Temperature: ".$temp."<br/>";
echo "Weather: ".$data['weather'][0]['main']."<br/>";
echo "Description: ".$data['weather'][0]['description']."";*/

?>
